//Based on Google search: "java echoServer"

import java.io.*;
import java.net.*;

public class client {
	public static void main(String[] args) throws IOException {
		//Below are hardcoded IP and port
		String host = "127.0.0.1";
		int port = 50000;
			
		try (
			Socket socket = new Socket(host, port);
			PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));	
		) {
			//HELO+AUTH code:
			out.println("HELO");
			System.out.println("Sent HELO");

			String buffer;
			int verified = 0;
			int jobID = 0;
			int iterations = 0; //for debug
			while ((buffer = in.readLine()) != null) {
				if (verified < 1 && buffer.equals("OK")) { //AUTH:
					out.println("AUTH COMP3100");
					verified++;
					System.out.println("Sent AUTH");
					iterations++;//
				} else if (verified > 0 && buffer.equals("OK")) { //IF AUTHed and received OK:
					iterations++;//
					System.out.println("verified = " + verified + " and iterations: " + iterations);//
					System.out.println("Sending REDY");
					out.println("REDY");
				} else if (buffer.equals("NONE")) { //IF NONE, then send QUIT to finish:
					System.out.println("Received NONE");
					out.println("QUIT");
					System.out.println("Sent QUIT");
					System.out.println("Jobs scheduled: " + jobID);
					break;
				} else if (serverCommand(buffer).equals("JOBN")) { //IF receiving a JOBN:
					out.println("SCHD " + jobID + " large 0");
					jobID++;
				} 

				//Todo:
				//Appendix B, Step 14: Exit
				//close connection if (received "QUIT")
				//if (buffer.equals("QUIT") { //put this before the line " .equals("JOBN")) { "
				//}
				//Techically, the "break" inside the IF "NONE" statement is what's 'closing the connection'
			}
		} catch (UnknownHostException e) {
			System.err.println("Host " + host + " not found.");
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Couldn't get I/O for the connection to " + host);
			System.exit(1);
		}
	}
	private static String serverCommand(String in) {
		if (in != null) {
			int index = in.indexOf(" ");
			System.out.println("Received command: " + in.substring(0, index));
			return in.substring(0, index);
		}
		return "Error in function serverCommand";
	}
}
