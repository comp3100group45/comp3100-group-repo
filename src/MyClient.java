import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;


public class MyClient {
    public static void main(String[] args) throws IOException {

        var socket = new Socket("localhost", 2222);
        PrintWriter pr = new PrintWriter(socket.getOutputStream());
        sendMessage("hello", pr);

        InputStreamReader in = new InputStreamReader(socket.getInputStream());
        BufferedReader bf = new BufferedReader(in);

        String str = bf.readLine();
        System.out.println("server : " + str);

//        if (recievedMessage("GDAY", bf)){
            sendMessage("do i still work", pr);
            System.out.println("sending a message");
//        }

        printedRecivedMessage(bf);

    }

    // for some reason this doesn't work in the client but works in the server
    static boolean recievedMessage(String expectedMessage, BufferedReader recievedMessage) throws IOException {
        return expectedMessage.equals(recievedMessage.readLine());
    }

    static void sendMessage(String message, PrintWriter writer){
        writer.println(message);
        writer.flush();
    }

    static void printedRecivedMessage(BufferedReader bf) throws IOException{
        System.out.println("server: " + bf.readLine());
    }
}