import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.lang.reflect.Array;
import java.net.Socket;
import java.util.*;

public class Client {
    private Socket _socket;
    private PrintWriter _out;
    private BufferedReader _in;

    private ArrayList<XmlServer> _xmlServers;

    // Options
    private final Algorithm _algorithm;
    private final Boolean _printMessages;

    public Client (HashMap options) {
        var algoMap = new HashMap<String, Algorithm>();
        algoMap.put("", Algorithm.AllToLargest);
        algoMap.put("wf", Algorithm.WorstFit);
        algoMap.put("bf", Algorithm.BestFit);
        algoMap.put("ff", Algorithm.FirstFit);
        _algorithm = algoMap.getOrDefault(options.getOrDefault("-a", ""), Algorithm.AllToLargest);

        _printMessages = options.containsKey("-v");
    }

    private class Job {
        public int SubmitTime;
        public int Id;
        public int EstRunTime;
        public int Cores;
        public int Memory;
        public int Disk;

        public Job (String[] params) {
            var config = Arrays.asList(params).subList(1, params.length).stream().mapToInt(Integer::parseInt).toArray();
            SubmitTime = config[0];
            Id = config[1];
            EstRunTime = config[2];
            Cores = config[3];
            Memory = config[4];
            Disk = config[5];
        }
    }

    private class BaseServer {
        public String Type;
        public int Cores;
        public int Memory;
        public int Disk;

        public Boolean CanDoJob(Job job) {
            return Cores >= job.Cores
                    && Memory >= job.Memory
                    && Disk >= job.Disk;
        }
    }

    private class Server extends BaseServer {
        public int Id;
        public int State;
        public int AvailableTime;

        public Server(String[] params) {
            var config = Arrays.asList(params).subList(1, params.length).stream().mapToInt(Integer::parseInt).toArray();
            Type = params[0];
            Id = config[0];
            State = config[1];
            AvailableTime = config[2];
            Cores = config[3];
            Memory = config[4];
            Disk = config[5];
        }

        public Server(XmlServer server) {
            Id = 0;
            State = 0;
            AvailableTime = 0;
            Type = server.Type;
            Cores = server.Cores;
            Memory = server.Memory;
            Disk = server.Disk;
        }
    }

    private class XmlServer extends BaseServer {
        public int Limit;
        public int BootupTime;
        public Double Rate;

        public XmlServer(Element el) {
            Type = el.getAttribute("type");
            Limit = Integer.parseInt(el.getAttribute("limit"));
            BootupTime = Integer.parseInt(el.getAttribute("bootupTime"));
            Rate = Double.parseDouble(el.getAttribute("rate"));
            Cores = Integer.parseInt(el.getAttribute("coreCount"));
            Memory = Integer.parseInt(el.getAttribute("memory"));
            Disk = Integer.parseInt(el.getAttribute("disk"));
        }
    }

    private enum Algorithm {
        AllToLargest,
        FirstFit,
        BestFit,
        WorstFit
    }

    private Algorithm GetAlgorithm(String val) {
        try {
            return Algorithm.valueOf(val);
        } catch (Exception ex) {
            return Algorithm.AllToLargest;
        }
    }

    public void Start(String ip, int port) throws IOException {
        try {
            // Connect to Server
            _socket = new Socket(ip, port);
            _out = new PrintWriter(_socket.getOutputStream(), true);
            _in = new BufferedReader(new InputStreamReader(_socket.getInputStream()));

            PrintMessage("Connected to Server");

            // Handshake and Auth
            Initialise();

            // Listen to Server
            Listen();

            // Stop Connection
            Stop();

        } catch (Exception ex) {
            Stop();
            throw ex;
        }
    }

    public void Stop() throws IOException {
        PrintMessage("Shutting down...");
        _in.close();
        _out.close();
        _socket.close();
    }

    private void Initialise() throws IOException {
        PrintMessage("Initialising...");
        // Initial Handshake
        String resp = SendMessage("HELO");
        if (!resp.equals("OK")) throw new IOException("Handshake Failed");

        // Authenticate with Server
        String clientName = GetUsername();
        resp = SendMessage("AUTH " + clientName);
        if (!resp.equals("OK")) throw new IOException("Authentication Failed");

        PrintMessage("Successfully Authenticated");

        _xmlServers = ParseServers();
        PrintMessage("Successfully read system.xml");
    }

    private void Listen() throws IOException {
        // Tell server we are listening
        _out.println("REDY");
        PrintMessage("Listening...");

        String buffer;
        while ((buffer = _in.readLine()) != null) {
            PrintMessage("RCVD: " + buffer);

            if (buffer.equals("NONE"))
                SendMessage("QUIT", false);

            if (buffer.equals("QUIT"))
                return;

            if (buffer.contains("JOBN")) {
                Job job = HandleJob(buffer);
                Server server;

                if (_algorithm == Algorithm.FirstFit) {
                	var servers = GetServers(job, "All");
                	server = FirstFit(job, servers);
                } else if (_algorithm == Algorithm.WorstFit) {
                    var servers = GetServers(job, "Capable");
                    server = WorstFit(job, servers);
                } else if(_algorithm == Algorithm.BestFit){
                    var servers = GetServers(job, "Capable");
                    server = BestFit(job, servers);
                } else {
                	//System.out.println("You shouldn't see this if baseline Alg is being used");
                    var servers = GetServers(job, "All");
                    server = AllToLargest(job, servers);
                }

                ScheduleJob(job, server);

                SendMessage("REDY", false);
            }
        }
    }
    
    private Server FirstFit (Job job, ArrayList<Server> servers) {
    	var serverList = new Hashtable<String, ArrayList<Server>>(); // Makes new Hashtable, server.Type is key, server is value
    	servers.forEach(server -> {
    		if (!serverList.containsKey(server.Type)) {
    			serverList.put(server.Type, new ArrayList<>());
    		}
    		serverList.get(server.Type).add(server);
    	});
    	
    	Collections.sort(_xmlServers, new Comparator<XmlServer>() { // Sorts servers by Cores
    		@Override
    		public int compare(XmlServer s1, XmlServer s2) {
    			int cores1 = s1.Cores;
    			int cores2 = s2.Cores;
    			
    			return cores1 - cores2;
    		}
    	});

    	Server firstFit = null;
    	Server lastResort = null;
    	
    	// Among servers with their initial resources from system.xml:
    	for (XmlServer xmlServer: _xmlServers) {
    		
    		// Pick the first "theoritically" sufficient server
    		if (xmlServer.CanDoJob(job) && lastResort == null) {
    			lastResort = new Server(xmlServer);
    		}
    		
    		// Skips loop if no sufficient server was found
        	if (!serverList.containsKey(xmlServer.Type)) {
        		continue;
        	}
        	for (Server server: serverList.get(xmlServer.Type)) {
        		if (server.CanDoJob(job) && server.State >= 0 && server.State <= 3) {
        			if (firstFit == null) { // If firstFit isn't found yet
        				firstFit = server;
        				continue; // ends loop if firstFit is found
        			}
        		}
        	}
        	if (lastResort != null) {
        		continue; // ends loop if both firstFit and lastResort is found 
        	}
    	}
    	if (firstFit != null) { //return firstFit if found
    		return firstFit;
    	} else if (lastResort != null) { //else return lastResort if it's found 
    		return lastResort;
    	}
    	return null;
    }
    
    private Server WorstFit(Job job, ArrayList<Server> servers) {
        // Group servers by type
        var groupedServers = new Hashtable<String, ArrayList<Server>>();
        servers.forEach(server -> {
            if (!groupedServers.containsKey(server.Type)) {
                groupedServers.put(server.Type, new ArrayList<>());
            }
            groupedServers.get(server.Type).add(server);
        });

        int worstFit = Integer.MIN_VALUE;
        int altFit = Integer.MIN_VALUE;
        int fbFit = Integer.MIN_VALUE;

        Server worstServer = null;
        Server altServer = null;
        Server fbServer = null;

        for (XmlServer xmlServer : _xmlServers) {
            // Find Fallback/Default Server
            var fbFitness = GetFitnessValue(job, xmlServer);
            if (fbFitness > fbFit && xmlServer.CanDoJob(job)) {
                fbFit = fbFitness;
                fbServer = new Server(xmlServer);
            }

            // Skip if no capable xml server found
            if (!groupedServers.containsKey(xmlServer.Type))
                continue;

            for (Server server : groupedServers.get(xmlServer.Type)) {
                var fitness = GetFitnessValue(job, server);

                if (server.CanDoJob(job)) {
                    if (fitness > worstFit && (server.State == 2 || server.State == 3)) {
                        worstFit = fitness;
                        worstServer = server;
                    } else if (fitness > altFit && (server.State == 0 || server.State == 1)) {
                        altFit = fitness;
                        altServer = server;
                    }
                }
            }
        }

        if (worstServer != null)
            return worstServer;
        else if (altServer != null)
            return altServer;
        else {
            return fbServer;
        }
    }


    private Server BestFit(Job job, ArrayList<Server> servers) {
        // Group servers by type
        var groupedServers = new Hashtable<String, ArrayList<Server>>();
        servers.forEach(server -> {
            if (!groupedServers.containsKey(server.Type)) {
                groupedServers.put(server.Type, new ArrayList<>());
            }
            groupedServers.get(server.Type).add(server);
        });

        int bestFit = Integer.MAX_VALUE;
        int minAvail = Integer.MAX_VALUE;
        int fbFit = Integer.MAX_VALUE;

        Server bestServer = null;
        Server fbServer = null;

        for (XmlServer xmlServer : _xmlServers) {
            // Find Fallback/Default Server
            var fbFitness = GetFitnessValue(job, xmlServer);
            if (fbFitness < fbFit && xmlServer.CanDoJob(job)) {
                fbFit = fbFitness;
                fbServer = new Server(xmlServer);
            }

            // Skip if no capable xml server found
            if (!groupedServers.containsKey(xmlServer.Type))
                continue;

            for (Server server : groupedServers.get(xmlServer.Type)) {
                var fitness = GetFitnessValue(job, server);

                if (server.CanDoJob(job)) {
                    if (fitness < bestFit || (fitness == bestFit && server.AvailableTime<minAvail)) {
                        bestFit = fitness;
                        bestServer = server;
                        minAvail = server.AvailableTime;
                    }
                }
            }
        }

        if (bestServer != null)
            return bestServer;
        else {
            return fbServer;
        }
    }

    private int GetFitnessValue(Job job, BaseServer server) {
        return server.Cores - job.Cores;
    }

    private Server AllToLargest(Job job, ArrayList<Server> servers) throws IOException {
        // Get largest server type
        String largestType = servers.get(servers.size() - 1).Type;

        // Get First Largest Server
        Optional<Server> largestServer = servers.stream().filter(server -> server.Type.equals(largestType)).findFirst();

        if (!largestServer.isPresent())
            throw new IOException("Failed to get largest server");

        return largestServer.get();
    }

    private ArrayList<Server> GetServers(Job job, String requestType) throws IOException {
        String resp = SendMessage(String.format("RESC %s %s %s %s", requestType, job.Cores, job.Memory, job.Disk));

        if (!resp.equals("DATA"))
            throw new IOException("JOB FAILED");

        SendMessage("OK", false);

        String buffer;
        var servers = new ArrayList<Server>();

        while (!(buffer = _in.readLine()).equals(".")) {
            PrintMessage("RCVD: " + buffer);
            String[] params = buffer.split(" ");
            Server server = new Server(params);
            servers.add(server);
            SendMessage("OK", false);
        }

        return servers;
    }

    private ArrayList<XmlServer> ParseServers() {
        var servers = new ArrayList<XmlServer>();
        try {
            var builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            var doc = builder.parse("system.xml");

            var nodeList = doc.getElementsByTagName("server");

            for (int i = 0; i < nodeList.getLength(); i++) {
                var node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    var el = (Element) node;
                    servers.add(new XmlServer(el));
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return servers;
    }

    private Job HandleJob(String jobStr) throws IOException {
        String[] params = jobStr.split(" ");
        Job job = new Job(params);

        return job;
    }

    private void ScheduleJob(Job job, Server server) throws IOException {
        String resp = SendMessage(String.format("SCHD %d %s %d", job.Id, server.Type, server.Id));

        if (!resp.equals("OK"))
            throw new IOException("SCHEDULE FAILED");
    }

    private String SendMessage(String msg) throws IOException {
        _out.println(msg);
        PrintMessage("Sent: " + msg);

        String resp = _in.readLine();
        PrintMessage("RCVD: " + resp);

        return resp;
    }

    public String SendMessage(String msg, Boolean getResponse) throws IOException {
        _out.println(msg.toCharArray());
        PrintMessage("Sent: " + msg);

        String resp = "";
        if (getResponse) {
            resp = _in.readLine();
            PrintMessage("RCVD: " + resp);
        }

        return resp;
    }

    private String GetUsername() {
        Map<String, String> env = System.getenv();
        if (env.containsKey("USERNAME"))
            return env.get("USERNAME");
        else if (env.containsKey("USER"))
            return env.get("USER");
        else
            return "Unknown User";
    }

    private void PrintMessage(String msg) {
        if (_printMessages)
            System.out.println(msg);
    }

    public static void main(String[] args) throws IOException {
        // Get options from args
        var options = new HashMap<String, String>();
        var currKey = "";
        for (int i = 0; i < args.length; i++) {
            if (args[i].charAt(0) == '-') {
                options.put(args[i], "");
                currKey = args[i];
            } else if (currKey != "") {
                options.replace(currKey, args[i]);
            }
        }

        //Init Client and Start
        Client client = new Client(options);
        client.Start("127.0.0.1", 50000);
    }
}
